use std::{
    env,
    net::{IpAddr, SocketAddr},
    str::FromStr,
    time::{SystemTime, UNIX_EPOCH},
};

use axum::{
    extract::{
        ws::{Message, WebSocket},
        WebSocketUpgrade,
    },
    routing::get,
    Router,
};

#[tokio::main]
async fn main() {
    // I know, this is a bit unidomatic... but it's simple and efficient
    let full_blurb: &'static str = Box::leak(Box::new(format!(
        "{} v{}\n\n{}",
        env!("CARGO_PKG_NAME"),
        env!("CARGO_PKG_VERSION"),
        include_str!("index.txt")
    )))
    .as_str();

    let app = Router::new()
        .route("/", get(move || async move { full_blurb }))
        .route(
            "/api/v1/ws",
            get(|ws: WebSocketUpgrade| async { ws.on_upgrade(handle_socket) }),
        );

    let address = get_listen_address();

    println!("Listening on {address:?}");

    axum::Server::bind(&address)
        .serve(app.into_make_service())
        .await
        .unwrap();
}

fn get_listen_address() -> SocketAddr {
    if let Ok(address) = env::var("BIND") {
        if let Ok(address) = address.parse() {
            return address;
        }
    }

    let bind_address = env::var("LISTEN_HOST")
        .ok()
        .and_then(|text| text.parse().ok())
        .unwrap_or(IpAddr::from_str("0.0.0.0").unwrap());
    let bind_port = env::var("PORT")
        .or_else(|_| env::var("LISTEN_PORT"))
        .ok()
        .and_then(|text| text.parse().ok())
        .unwrap_or(3000);

    SocketAddr::new(bind_address, bind_port)
}

async fn handle_socket(mut socket: WebSocket) {
    while let Some(msg) = socket.recv().await {
        let now = SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .unwrap_or_default()
            .as_millis();

        let msg = if let Ok(msg) = msg {
            msg
        } else {
            // client disconnected
            return;
        };

        let reply_msg = match msg {
            Message::Text(_) => Message::Text(format!("{}", now)),
            Message::Binary(_) => Message::Binary(now.to_le_bytes().to_vec()),
            Message::Close(_) => {
                return;
            }
            _ => {
                continue;
            }
        };

        if socket.send(reply_msg).await.is_err() {
            // client disconnected
            return;
        }
    }
}
